#include "BlacknWhiteFilter.hpp"
#include <iostream>
BlacknWhiteFilter::BlacknWhiteFilter(){
}
BlacknWhiteFilter::~BlacknWhiteFilter(){
}
void BlacknWhiteFilter::appFilters(Image & img)
{
      int x, z;
      Image* image = &img;
      for(x= 0; x < image->getWidth(); ++x){
        for(z= 0; z < image->getHeight();++z){
          int grayscale  = (0.299 * image->pixel[x][z][1]) + (0.587 * image->pixel[x][z][2])+(0.144 * image->pixel[x][z][3]);
if(grayscale <=255){
            image->pixel[x][z][1]= grayscale;
            image->pixel[x][z][2]= grayscale;
            image->pixel[x][z][3]= grayscale;
            }
else{
image->pixel[x][z][1]=image->getFullcolor();
image->pixel[x][z][2]=image->getFullcolor();
image->pixel[x][z][3]=image->getFullcolor();
}
      }
    }
}
