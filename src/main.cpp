#include "Image.hpp"
#include "NegativeFilter.hpp"
#include "PolarizedFilter.hpp"
#include "BlacknWhiteFilter.hpp"
#include "MeanFilter.hpp"
string imageCopy,  imageFile;
Image* image;
int main (){
  void appNegativeFilter();
  void appPolarizedFilter();
  void appBlacknWhiteFilter();
  void appMeanFilter();
  int option;

      cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" << endl;
      cout << "\t\t\t\v    FILTROS DE IMAGEM PPM    \v\t\t\n" << endl;
      cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-"<<endl;
      cout << "\n #INSTRUÇÕES:\n"<< endl; 
      cout << "\n # Escolha o filtro que deseja aplicar à sua imagem\n" << endl;
      cout << "\n # Escolha uma imagem .ppm no diretório /doc.\n"<< endl;
      cout <<"\n  # Escolha um nome para a nova imagem á ser criada" <<endl; 
      cout << "\n # A nova imagem será salva na pasta /doc.\n" << endl;
      cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
      cout << "\n # Digite o numero referente ao filtro que deseja aplicar a sua imagem .ppm \n\n" <<endl;
      cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
      cout << "[1]-Filtro Negativo \n\t"<<endl;
      cout << "[2]-Filtro Polarizado\n\t"<<endl;
      cout << "[3]-Filtro Preto e Branco \n\t"<<endl;
      cout << "[4]-Filtro Media \n\t"<<endl;
      cout << "[5]-Sair\n\t"<<endl;
      cin >> option;
      cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
      cout << endl;
switch (option) {
          case 1:{
	      cout << "\n # Digite o nome do arquivo .ppm a ser editado (sem extensão '.ppm'):\t ";
              cin >> imageFile;
              image = new Image(imageFile);
              cout << endl;
              cout << "# Qual será o nome da nova imagem? " <<endl;
                cin >> imageCopy;
                 appNegativeFilter();
            break;
          }
          case 2:{
 		cout << "\n # Digite o nome do arquivo .ppm a ser editado (sem extensão '.ppm'):\t ";
                cin >> imageFile;
                image = new Image(imageFile);
                cout << endl;

                 cout << "# Qual será o nome da nova imagem? " <<endl;
                 cin >> imageCopy;
                  appPolarizedFilter();
            break;
          }
          case 3:{
                  cout << "\n # Digite o nome do arquivo .ppm a ser editado (sem extensão '.ppm'):\t ";
                  cin >> imageFile;
                  image = new Image(imageFile);
                   cout << endl;
                  cout << "# Qual será o nome da nova imagem? " <<endl;
                  cin >> imageCopy;
                appBlacknWhiteFilter();
          break;
        }
        case 4:{
              cout << "\n # Digite o nome do arquivo .ppm a ser editado (sem extensão '.ppm'):\t ";
              cin >> imageFile;
              image = new Image(imageFile);
              cout << endl;
               cout << "# Qual será o nome da nova imagem? " <<endl;
            cin >> imageCopy;
              appMeanFilter();
        break;}
        
	case 5: {
	   cout <<"# Obrigado por utilizar o programa! Execute-o novamente caso queira editar um novo arquivo!\n"<<endl;
	  return 0;
         break;}

	default:
          cout << "\a# Opção Inválida!" << endl;
            break;
}

  delete (image);
    return 0;
}
void appNegativeFilter(){
  NegativeFilter nf;
  nf.appFilters(*image);
  cout<<image->WritePixel(imageCopy) <<endl;
}
void appPolarizedFilter(){
  PolarizedFilter pf;
 	 pf.appFilters(*image);
 		 cout<<image->WritePixel(imageCopy)<<endl;
}
void appBlacknWhiteFilter(){
  BlacknWhiteFilter bwf;
 	 bwf.appFilters(*image);
 		cout<<image->WritePixel(imageCopy)<< endl;
}
void appMeanFilter(){
int size;
cout<< endl;
cout<< "\n #Digite a opção da dimensão que você deseja aplicar a sua imagem:"<< endl;
cout<< "[1] 3x3\n"<< endl;
cout<< "[2] 5x5\n"<< endl;
cout<< "[3] 7x7\n"<< endl;
cin>> size;
cout << endl;
MeanFilter mf;
switch(size){
case 1:{
  mf.appFilters(*image, size);
  		cout << image->WritePixel(imageCopy) << endl;
  		break;
}
case 2:{
  mf.appFilters(*image, size);
  		cout << image->WritePixel(imageCopy) << endl;
  		break;}
case 3:{
   mf.appFilters(*image, size);
    		cout << image->WritePixel(imageCopy) << endl;
    		break;
}
default:
cout<<"\a # Opção inválida!"<<endl;
appMeanFilter();
break;
}
}
