#ifndef IMAGE_HPP
#define IMAGE_HPP
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
  class Image {
    private:
          ifstream read;
          ofstream write;

      int width;
      int height;
      int fullcolor;
      string magic_number;
      string Origin;
      string Copy;
          

      void readHeader();
      void readPixel();
    public:
      Image(string imageFile);
      ~Image();
      unsigned char pixel[1000][1000][4];
      string WritePixel(string imageCopy);
        string getMagic_number();
          void setMagic_number(string magic_number);
          int getWidth();
          void setWidth(int width);
          int getHeight();
          void setHeight(int height);
	   int getFullcolor();
          void setFullcolor(int fullcolor);
};
#endif
