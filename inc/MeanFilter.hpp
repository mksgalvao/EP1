#ifndef MEANFILTER_HPP
#define MEANFILTER_HPP
#include "Filters.hpp"
class MeanFilter  : public Filters{
public:
 MeanFilter();~MeanFilter();
  
  void appFilters(Image & img,int Size);
};
#endif
