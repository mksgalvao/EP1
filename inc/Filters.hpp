#ifndef FILTERS_HPP
#define FILTERS_HPP
#include "Image.hpp"
using namespace std;
class Filters{
  public:
    Filters();
    ~Filters();
    void appFilters(Image & image);
};
#endif
