#ifndef BLACKNWHITEFILTER_HPP
#define BLACKNWHITEFILTER_HPP
#include "Filters.hpp"
class BlacknWhiteFilter : public Filters{
public:
  BlacknWhiteFilter();
  ~BlacknWhiteFilter();
  void appFilters(Image &image);
};
#endif
