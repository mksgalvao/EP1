# EP1 - 25/09/2016 (UnB - Gama)
Mônica Karoline Silva Galvão matricula: 14/0048103

Esse programa tem por objetivo aplicar filtros em imagens do tipo ppm. 

Ele é composto dos seguintos diretórios:

bin  Diretório onde ficam os arquivos binários 
inc: Diretório que contém as bibliotecas.
src: Diretório com a implementação das bibliotecas.
obj: Diretório que recebe os objetos após compilado.
doc: Diretório contendo as imagens a serem utilizadas neste trabalho.
Elas são : flores.ppm, terra.ppm, dados.ppm, unb.ppm, frutas.ppm, xadrez.ppm

Neste projeto foi implementado o arquivo Makefile.
Para compilar o programa abra o terminal no diretório EP1 e digite "make". Para executar o programa, basta digitar: "make run" 
Caso queira remover os objetos e o binário do diretório digite: "make clean".

Após executá-lo o programa solicita que o usuário entre com a opção de filtro que se deseja aplicar a imagem em um menu simples, onde basta digitar o número correspondente ao filtro desejado ou a opçao para sair do menu.O programa irá solicitar o nome da imagem ppm a ser editada. Lembrando que não é necessário colocar a extensão do arquivo. Depois de digitar um nome válido nas opções de imagens disponíveis, que foram disponibilizadas no diretório /doc e caso não ocorra nenhum erro na abertura da imagem o programa ira pedir um novo nome para a nova imagem, digite apenas o nome, sem extensão, por exemplo: frutas2
Feito isso, a imagem será salva na pasta /doc. 
